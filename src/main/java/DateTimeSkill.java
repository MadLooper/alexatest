package main.java;

import java.time.LocalDateTime;


/**
 * Created by KW on 9/26/2017.
 */
public class DateTimeSkill extends AbstractSkill {

    public DateTimeSkill() {

    }

    public void run() {
        System.out.println("Today's "+ LocalDateTime.now());
    }
}
