package main.java;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

/**
 * Created by KW on 9/26/2017.
 */
public class Request implements Observable{
    private String request;
    private AlexaDevice alexaDevice;

    public Request(String request, AlexaDevice alexaDevice) {
        this.request = request;
        this.alexaDevice = alexaDevice;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public AlexaDevice getAlexaDevice() {
        return alexaDevice;
    }

    public void setAlexaDevice(AlexaDevice alexaDevice) {
        this.alexaDevice = alexaDevice;
    }

    public void addListener(InvalidationListener listener) {

    }

    public void removeListener(InvalidationListener listener) {

    }
}
