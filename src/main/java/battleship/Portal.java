package main.java.battleship;

import java.util.ArrayList;

/**
 * Created by KW on 9/27/2017.
 */
public class Portal {
    String name;
    ArrayList<String> pola = new ArrayList<>();

    public Portal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getPola() {
        return pola;
    }

    public void setPola(String e) {
       pola.add(e);
    }

    public void findPola (String s){
        if (pola.isEmpty()){
            System.out.println("Okret zatopiony");
        }
       if( pola.contains(s)){
           System.out.println("Zatopiony");
           pola.remove(s);
       }else {
           System.out.println("Pudlo");
       }
    }
}
