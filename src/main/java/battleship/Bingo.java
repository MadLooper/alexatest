package main.java.battleship;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by KW on 9/29/2017.
 */
public class Bingo {

    private String color;

    public Bingo(String color) {
        this.color = color;
    }

    public String getColor() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("yellow");
        colors.add("white");
        colors.add("purple");
        colors.add("red");

        int idx = new Random().nextInt(colors.size());
        String random = (colors.get(idx));

        return random;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public static String[] getCard() {
        String[] bingo =new String [5];
String  str = "BINGO";
        Random random = new Random();
     bingo[0] = "B" + random.nextInt(16);
        bingo[1] = "I" + random.nextInt(30-16)+16;
        bingo[2] = "N" + random.nextInt(45-31)+31;
        bingo[3] = "G" + random.nextInt(60-46)+46;
        bingo[4] = "O" +  random.nextInt(75-61)+75;
        return bingo;
    }

    public static void main(String[] args) {
        Bingo bingo = new Bingo();
        for (String s:getCard() )
        System.out.println(s);
    }
}


